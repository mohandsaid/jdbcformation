package com.innob.jdbc.service;

import com.innob.jdbc.model.Employee;

import java.util.List;
import java.util.Map;

public interface IEmployeeServices {

    Employee getEmployeeById(Long employeeId);


    Employee getEmployeeByLastName(String lastName);

    List<Employee> getEmployeesByLastNameAutoCompletion(String lastName);

    List<Employee> getEmployees();

    Map<String, Boolean> delete(Long employeeId);

    Employee getEmployeeByNumSecu(Long numSecu);

    List<Employee> getMajorEmployees();

    Map<String, Boolean> save(Employee employee);

    Map<String, Boolean> updateEmployee(Long employeeId, Employee employee);
}
