package com.innob.jdbc.service;

import com.innob.jdbc.dao.jdbc.DBUtil;
import com.innob.jdbc.dao.jdbc.IEmployeeDao;
import com.innob.jdbc.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeServices implements IEmployeeServices {

    static Logger log = LoggerFactory.getLogger(EmployeeServices.class) ;

    public EmployeeServices(){
        log.info("EmployeeServices.constructor");
    }

    @Autowired  //check avec Rabah
    private IEmployeeDao employeeDao;


    public Employee getEmployeeById(Long employeeId) {
        return employeeDao.getEmployeeById(employeeId);
    }


    public Employee getEmployeeByLastName(String lastName) {
        return employeeDao.getEmployeeByLastName(lastName);
    }

    public List<Employee> getEmployeesByLastNameAutoCompletion(String lastName) {
        return employeeDao.getEmployeesByLastNameAutoCompletion(lastName);
    }

    @Override
    public List<Employee> getEmployees() {
        return employeeDao.getEmployees();
    }

    public Map<String, Boolean> delete (Long employeeId){
        Map<String, Boolean> response = new HashMap<>();
        if (employeeDao.getEmployeeById(employeeId) == null){
            response.put("deleted", Boolean.FALSE);
            return response;
        }
        employeeDao.deleteEmployee(employeeId);
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    public Employee getEmployeeByNumSecu(Long numSecu) {
        return employeeDao.getEmployeeByNumSecu(numSecu);
    }

    public List<Employee> getMajorEmployees() {
    return employeeDao.getMajorEmployees();
    }

    public Map<String, Boolean> save(Employee employee) {
        Map<String, Boolean> response = new HashMap<>();
        if (!DBUtil.isNumSecuExist(employee.getNumSecu())) {
            response.put("saved", employeeDao.SaveEmployee(employee));
            return response;
        }
        response.put("Employee with this num secu is already exists. | saved", false);
        return response;
    }

    public Map<String, Boolean> updateEmployee(Long employeeId, Employee employee) {
        Map<String, Boolean> response = new HashMap<>();
        if (getEmployeeById(employeeId)!=null) {
            response.put("updated", employeeDao.UpdateEmployee(employeeId, employee));
            return response;
        }
        response.put("Employee does not exist. | updated", false);
        return response;
    }
}
