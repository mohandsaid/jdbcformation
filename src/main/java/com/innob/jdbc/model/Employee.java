package com.innob.jdbc.model;

import com.innob.jdbc.helper.models.Gender;

import java.util.Date;
import java.util.Objects;

public class Employee {

    private Long id;
    private String lastName;
    private String firstName;
    private int age;
    private Gender gender;
    private Date birthDate;
   private Long numSecu;

    public Employee() {

    }

    public Employee(Long id, String lastName, String firstName, int age, Gender gender, Date birthDate, Long numSecu) { //List<Person> friends) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.gender = gender;
        this.birthDate = birthDate;
        this.numSecu = numSecu;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) { this.gender = gender; }

    public Date getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Long getNumSecu() { return numSecu; }
    public void setNumSecu(Long numSecu) { this.numSecu = numSecu; }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                ", numSecu=" + numSecu +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return getAge() == employee.getAge() &&
                getId().equals(employee.getId()) &&
                Objects.equals(getLastName(), employee.getLastName()) &&
                Objects.equals(getFirstName(), employee.getFirstName()) &&
                getGender() == employee.getGender() &&
                Objects.equals(birthDate, employee.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLastName(), getFirstName(), getAge(), getGender(), birthDate);
    }
}