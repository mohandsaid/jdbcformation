package com.innob.jdbc;

import com.innob.jdbc.dao.jdbc.DBUtil;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class JDBC_formationApplication {

	public static void initEnv() {
		DBUtil.chargeDriver();
	}


	public static void main(String[] args) {
		initEnv();
		SpringApplication.run(JDBC_formationApplication.class, args);
	}
}