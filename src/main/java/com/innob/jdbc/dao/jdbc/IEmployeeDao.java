package com.innob.jdbc.dao.jdbc;

import com.innob.jdbc.model.Employee;

import java.util.List;

public interface IEmployeeDao {
    Employee getEmployeeByLastName(String lastName);

    List<Employee> getEmployeesByLastNameAutoCompletion(String lastName);

    Employee getEmployeeById(Long identifiant);

    List<Employee> getMajorEmployees();

    Employee getEmployeeByNumSecu(Long numSecu);

    boolean deleteEmployee(Long employeeId);

    boolean SaveEmployee(Employee employee);

    boolean UpdateEmployee(Long employeeId, Employee employee);

    List<Employee> getEmployees();
}
