package com.innob.jdbc.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

import static com.innob.jdbc.dao.jdbc.JdbcConst.*;
import static com.innob.jdbc.dao.jdbc.JdbcConst.PASSWORD;

public class DBUtil {
    static Logger log = LoggerFactory.getLogger(EmployeeDao.class);
    public static void chargeDriver(){
        try {
            Class.forName(JDBC_DRIVER.getValue());
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
        }
    }

    public static Long getNewId(){
        Long newId=0L;
        try (Connection conn = DriverManager.getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("SELECT MAX(id) max_id FROM employee;");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                newId = rs.getLong("max_id")+1L;
            }
        } catch (SQLException se) {
            log.error(se.getMessage());
        }
        return newId;
    }

    public static boolean isNumSecuExist(Long numSecu){
        boolean isExist = false;
        try(Connection conn = DriverManager.getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())){
            PreparedStatement stmt = conn.prepareStatement("select count(*) nbr_ns FROM employee where num_secu = ?");
            stmt.setLong(1, numSecu);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                isExist = rs.getInt("nbr_ns") > 0;
            }
        }catch (SQLException se) {
            log.error(se.getMessage());
        }
        return isExist;
    }
}
