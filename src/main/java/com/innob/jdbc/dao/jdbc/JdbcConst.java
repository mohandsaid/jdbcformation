package com.innob.jdbc.dao.jdbc;

public enum JdbcConst {
    SEMICOLON(";"),
    JDBC_DRIVER("org.postgresql.Driver"),
    URL("jdbc:postgresql://localhost:5432/formation"),
    USER("innob"),
    PASSWORD("2019Innob");

    public String value ;

    JdbcConst(String value) {
        this.value = value;
    }

    public String getValue() { return value; }
}
