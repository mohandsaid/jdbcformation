package com.innob.jdbc.dao.jdbc;

import com.innob.jdbc.helper.models.Gender;
import com.innob.jdbc.helper.utils.DateUtils;
import com.innob.jdbc.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.innob.jdbc.dao.jdbc.JdbcConst.*;
import static java.sql.DriverManager.getConnection;

@Repository
public class EmployeeDao implements IEmployeeDao {

    static Logger log = LoggerFactory.getLogger(EmployeeDao.class);

    public EmployeeDao(){
        log.info("EmployeeDao.constructor");
    }

    // Get a employee with exact value using hash index
    public Employee getEmployeeByLastName(String lastName) {
        return getEmployeesByString("last_name", lastName).get(0);
    }

    //Get employees with approximative value of their last name
    public List<Employee> getEmployeesByLastNameAutoCompletion(String lastName) {
        return getEmployeesByStringAutoCompleted("last_name", lastName);
    }

    // Get a employee with exact value using hash index
    public Employee getEmployeeById(Long identifiant) {
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("SELECT * from employee where id = ?");
            stmt.setLong(1, identifiant);
            ResultSet rs = stmt.executeQuery();
            return mapToEmployee(rs);
        } catch (SQLException se) {
            log.error("Error lors de getEmployeeById de la table employee  avec id : " + identifiant,se);
        }
        return null;
    }


    public List<Employee> getMajorEmployees() {
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("SELECT * from employee where age >= 18 LIMIT 20");
            ResultSet rs = stmt.executeQuery();
            return mapToListEmployee(rs);
        } catch (SQLException se) {
            log.info("Error lors de l'appel de la methode getMajorEmployees" +
                    "()",se);
        }
        return null;
    }

    // Get a employee with exact value using hash index
    public Employee getEmployeeByNumSecu(Long numSecu) {
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("SELECT * from employee where num_secu = ?");
            stmt.setLong(1, numSecu);
            ResultSet rs = stmt.executeQuery();
                return mapToEmployee(rs);
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode getEmployeesByLong()",se);
        }
        return null;
    }


    // Get empployees with exact value using btree index
    public List<Employee> getEmployeesByString(String columnName, String stringElement) {
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("SELECT * from employee where " + columnName + " = ? ");
            stmt.setString(1, stringElement);
            ResultSet rs = stmt.executeQuery();
            return mapToListEmployee(rs);
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode getEmployeesByString()",se);
        }
        return null;
    }

    // Get employees with exact value using btree index
    public List<Employee> getEmployeesByStringAutoCompleted(String columnName, String stringElement) {
        List<Employee> employees = new ArrayList<>();
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("SELECT * from employee where " + columnName + " >= ? and " + columnName + " <= ? LIMIT 20");
            stmt.setString(1, stringElement);
            stmt.setString(2, stringElement + "z");
            ResultSet rs = stmt.executeQuery();
            return mapToListEmployee(rs);
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode getEmployeesByStringAutoCompleted()",se);
        }
        return null;
    }

    //Map To Employees.
    private List<Employee> mapToListEmployee(ResultSet rs) throws SQLException {
        List<Employee> employees = new ArrayList<>();
        while (rs.next()) {
            Employee employee = new Employee();
            Long id = rs.getLong("id");
            int age = rs.getInt("age");
            String firstName = rs.getString("first_name");
            String lastName = rs.getString("last_name");
            String gender = rs.getString("gender");
            Date birthDate = rs.getDate("birth_date");
            Long numSecu = rs.getLong("num_secu");
            //map to employees
            employee.setId(id);
            employee.setAge(age);
            employee.setLastName(lastName);
            employee.setFirstName(firstName);
            employee.setGender(Gender.valueOf(gender));
            employee.setNumSecu(numSecu);
            employee.setBirthDate(birthDate);
            employees.add(employee);
        }
        return employees;
    }

    //Map To Employee.
    private Employee mapToEmployee(ResultSet rs) throws SQLException {
        Employee employee = null ;
        if (rs.next()) {
            employee = new Employee();
            //map to employees
            employee.setId(rs.getLong("id"));
            employee.setAge(rs.getInt("age"));
            employee.setLastName(rs.getString("last_name"));
            employee.setFirstName(rs.getString("first_name"));
            employee.setGender(Gender.valueOf(rs.getString("gender")));
            employee.setNumSecu(rs.getLong("num_secu"));
            employee.setBirthDate(rs.getDate("birth_date"));
        }
        return employee;
    }


    public int updatenum_secu(Long i, Long iden) {
        log.info("Connecting to database...");
        int rs =0;
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("update employee set num_secu = " + i + " WHERE id = " + iden + ";");
            rs = stmt.executeUpdate();
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode updatenum_secu()",se);
        }
        return rs;
    }

    public static void updateAge(int i, Long iden) {
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("update employee set age = " + i + " WHERE id = " + iden + ";");
            int rs = stmt.executeUpdate();
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode updateAge()",se);

        }
    }



    public boolean deleteEmployee(Long employeeId) {
        int response = 0;
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("delete FROM employee where id = ?");
            stmt.setLong(1, employeeId);
            response = stmt.executeUpdate();
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode delete()",se);
        }
        return response > 0;
    }

    public boolean SaveEmployee(Employee employee) {
        int response = 0;
        log.info("Connecting to database for saving employee...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO employee VALUES (?, ?, ?, ?, ?, ?, ?)");

            stmt.setLong(1, DBUtil.getNewId());
            stmt.setInt(2, employee.getAge());
            stmt.setDate(3, DateUtils.convertToSqlDate(employee.getBirthDate()));
            stmt.setString(4, employee.getFirstName());
            stmt.setString(5, String.valueOf(employee.getGender()));
            stmt.setString(6, employee.getLastName());
            stmt.setLong(7, employee.getNumSecu());
            response = stmt.executeUpdate();
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode SaveEmployee()",se);
        }
        return response > 0;
    }

    public boolean UpdateEmployee(Long employeeId, Employee employee) {
        int response = 0;
        log.info("Connecting to database...");
        try (Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue())) {
            PreparedStatement stmt = conn.prepareStatement("update employee SET first_name = ?, last_name = ? , gender = ?, age = ?, birth_date = ? WHERE id=?;");

            stmt.setString(1, employee.getFirstName());
            stmt.setString(2, employee.getLastName());
            stmt.setString(3, String.valueOf(employee.getGender()));
            stmt.setInt(4, employee.getAge());
            stmt.setDate(5, DateUtils.convertToSqlDate(employee.getBirthDate()));
            stmt.setLong(6, employeeId);
            response = stmt.executeUpdate();
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode SaveEmployee()",se);
        }
        return response > 0;
    }

    public List<Employee> getEmployees() {
        log.info("Connecting to database for getEmployees ...");
        try {
            Connection conn = getConnection(URL.getValue(), USER.getValue(), PASSWORD.getValue());
            PreparedStatement stmt = conn.prepareStatement("SELECT * from employee");
            ResultSet rs = stmt.executeQuery();
            return mapToListEmployee(rs);
        } catch (SQLException se) {
            log.error("Error lors de l'appel de la methode getEmployees()",se);
        }
        return null;
    }
}
