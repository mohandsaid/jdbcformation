package com.innob.jdbc.controller;

import com.innob.jdbc.model.Employee;
import com.innob.jdbc.service.IEmployeeServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/innob")

//@Scope("singleton")
@Scope("request")
public class EmployeeController {

    @Autowired
    private IEmployeeServices employeeServices;

    static Logger log = LoggerFactory.getLogger(EmployeeController.class);

    public EmployeeController(IEmployeeServices employeeServices) {
        this.employeeServices = employeeServices;
        log.info("EmployeeController.constructor");
    }

    @GetMapping("/employees/search/{autoComplet}")
    public List<Employee> getAllEmployees(@PathVariable(value = "autoComplet") String lastName) {
        return employeeServices.getEmployeesByLastNameAutoCompletion(lastName);
    }

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getEmployees() {
    return ResponseEntity.ok().body(employeeServices.getEmployees());
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId) {
        Employee employee = employeeServices.getEmployeeById(employeeId);
       // if(person==null){return (ResponseEntity<Person>) ResponseEntity.notFound();}
        return ResponseEntity.ok().body(employee);
    }

    @GetMapping("/employees/lastName/{lastName}")
    public ResponseEntity<Employee> getEmployeeByLastName(@PathVariable(value = "lastName") String lastName) {
        Employee employee = employeeServices.getEmployeeByLastName(lastName);
        return ResponseEntity.ok().body(employee);
    }

    @GetMapping("/employees/securityNumber/{numSecu}")
    public ResponseEntity<Employee> getEmployeeByNumSecu(@PathVariable(value = "numSecu") Long numSecu) {
        Employee employee = employeeServices.getEmployeeByNumSecu(numSecu);
        return ResponseEntity.ok().body(employee);
    }

    @GetMapping("/employees/majors")
    public List<Employee> getMajorEmployees() {
        return employeeServices.getMajorEmployees();
    }

    @PostMapping("/employee")
    public Map<String, Boolean> createEmployee(@Valid @RequestBody Employee employee) {
        return employeeServices.save(employee);
    }

    @DeleteMapping("/employee/delete/{employeeId}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "employeeId") Long employeeId) {
        return employeeServices.delete(employeeId);
    }

/*    @PostMapping("/csv")
    public ResponseEntity<String> importCsv(@Valid @RequestBody String chemin) {
       ImportCSV.readCsvFile(chemin).forEach(person -> personRepository.save(person));
        return ResponseEntity.ok("import csv ok");
    }*/

    @PutMapping("/employee/update/{id}")
    public Map<String, Boolean> updateEmployee(@PathVariable(value = "id") Long employeeId, @Valid @RequestBody Employee employee) {
        return employeeServices.updateEmployee(employeeId, employee);
    }
/*
    @DeleteMapping("/persons/{id}")
    public Map<String, Boolean> deletePerson(@PathVariable(value = "id") Long personId)
            throws ResourceNotFoundException {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person not found for this id :: " + personId));

        personRepository.delete(person);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }*/
}