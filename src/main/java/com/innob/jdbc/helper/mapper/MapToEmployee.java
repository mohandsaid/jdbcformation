package com.innob.jdbc.helper.mapper;

import com.innob.jdbc.helper.models.Gender;
import com.innob.jdbc.helper.utils.DateUtils;
import com.innob.jdbc.model.Employee;

import java.text.ParseException;

public class MapToEmployee {

    private static final String SEMICOLON = ";";

    // TODO: Ajouter les autres cloumnes une fois le code fonctionne.

    public static Employee mapToPerson(String ligne) {
        String[] p = ligne.split(SEMICOLON);
        Employee element = new Employee();
        try {
            element.setId(Long.valueOf(p[0]));
            element.setLastName(p[1]);
            element.setFirstName(p[2]);
            element.setGender(Gender.valueOf(p[3].replaceAll("\\s+", "")));
            element.setAge(Integer.parseInt(p[4].replaceAll("\\s+", ""))); //delete whitespaces
            element.setBirthDate(DateUtils.getDate(p[5].replaceAll("\\s+", "")));
//            element.setBirthDate(DateUtils.getDate("20190825".replaceAll("\\s+", "")));
            //  element.setFriends(mapToList(p[6]));
        } catch (ParseException e) {
            e.getMessage();
        }
        return element;
    }
}
