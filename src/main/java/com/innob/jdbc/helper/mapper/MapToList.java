package com.innob.jdbc.helper.mapper;

import com.innob.jdbc.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class MapToList {
    private static final String COMMA = ",";

    public static List<Employee> mapToList(String stringList) {
        String[] friendId = stringList.split(COMMA);
        Employee element = new Employee();
        List<Employee> response = new ArrayList<>();
        for (String id : friendId) {
            element.setId(Long.valueOf(id.replaceAll("\\s+","")));
            response.add(element);
        }
        return response;
    }
}
