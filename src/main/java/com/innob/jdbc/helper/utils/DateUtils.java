package com.innob.jdbc.helper.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {

    private static final DateTimeFormatter DATE_TIME_PATTERN = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MMM/yyyy");;

    //TODO : géré=er les execeptions
public static Date getDateFromString(String stringDate) throws ParseException {
        Date date = SIMPLE_DATE_FORMAT.parse(stringDate);
        return date;
    }

    public static Date getDate(String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Date date = df.parse(dateString);
        return date;
    }

    public static String currentDateTime(){
        return LocalDateTime.now().format(DATE_TIME_PATTERN) ;
    }

    public static String convertDateToString(Date inDate)
    {
        String dateString = null;
        try{
            dateString = SIMPLE_DATE_FORMAT.format( inDate );
        }catch (Exception e ){
        }
        return dateString;
    }

    // Function to convert java.util.Date to java.sql.Date
    public static java.sql.Date convertToSqlDate(java.util.Date date)
    {
        // java.util.Date contains both date and time information
        // java.sql.Date contains only date information (without time)
        return new java.sql.Date(date.getTime());
    }
}

