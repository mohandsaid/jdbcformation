package com.innob.jdbc.helper.utils;

import java.util.Random;

public class InnobUtils {
    public static int getRandomInt(int min, int max){
        Random rand = new Random();
        int n = rand.nextInt(max) - min;
        if (n <= 0) {return 15;}
        return n;
    }
}
